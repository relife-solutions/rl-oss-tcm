namespace ReLife.TCM.Extensions.Tests;

public class LensesTests
{
    public class TestClass
    {
        public int Property { get; set; }
        public int Field { get; set; }

        public int Method() => 42;
    }

    [Test]
    public void CreatingLensSucceedsForPropertyAndField()
    {
        MutLenses<TestClass>.For(x => x.Property).HasValue.Should().BeTrue();
        MutLenses<TestClass>.For(x => x.Field).HasValue.Should().BeTrue();
    }

    [Test]
    public void CreatingLensForMethodShouldFail()
    {
        MutLenses<TestClass>.For(x => x.Method()).HasValue.Should().BeFalse();
    }

    [Test]
    public void LensGetterAccessesCorrectData()
    {
        var item = new TestClass {Property = 1, Field = 2};
        
        var prop = MutLenses<TestClass>.For(x => x.Property).Value; 
        var field = MutLenses<TestClass>.For(x => x.Field).Value;

        prop.Get(item).Should().Be(item.Property);
        field.Get(item).Should().Be(item.Field);
    }

    [Test]
    public void LensSetterSetsData()
    {
        var item = new TestClass {Property = 1, Field = 2};
        
        var prop = MutLenses<TestClass>.For(x => x.Property).Value; 
        var field = MutLenses<TestClass>.For(x => x.Field).Value;

        var itemP = prop.SetF(4)(item);
        itemP.Property.Should().Be(4);
        
        var itemF = field.SetF(4)(item);
        itemF.Field.Should().Be(4);
    }
}