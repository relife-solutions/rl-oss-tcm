import Case from '../app/src/components/Case.svelte';

export default {
    title: 'Components/Case',
    component: Case,
    argTypes: {
        testCase: {
            control: 'object'
        }
    }
};

const Template = args => ({
    Component: Case,
    props: {
        testCase: args.testCase
    }
});

export const Default = Template.bind({});
Default.args = {
    testCase: {
        title: 'some title',
        tags: ['a', 'b', 'c'],
        steps: [{}, {}, {}],
        testData: ['', '', ''],
    }
};

export const EmptyStepsAndData = Template.bind({});
EmptyStepsAndData.args = {
    testCase: {
        title: 'some title',
        tags: ['a', 'b', 'c'],
        steps: [],
        testData: [],
    }
};


export const EmptyTags = Template.bind({});
EmptyTags.args = {
    testCase: {
        title: 'some title',
        tags: [],
        steps: [{}, {}, {}],
        testData: ['', '', ''],
    }
};