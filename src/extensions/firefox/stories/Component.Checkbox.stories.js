import Checkbox from '../app/src/components/Checkbox.svelte';

export default {
    title: 'Components/Checkbox',
    component: Checkbox,
    argTypes: {
        onToggled: { action: 'toggled' },
        checked: { control: 'boolean' }
    }
};

const Template = args => ({ Component: Checkbox, props: args });

export const Default = Template.bind({});
