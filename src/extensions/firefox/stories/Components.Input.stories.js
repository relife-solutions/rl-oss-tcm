import Input from '../app/src/components/Input.svelte';

export default {
    title: 'Components/Input',
    component: Input,
    argTypes: {
        label: { control: 'text' },
        name: { control: 'text' },
        type: { control: 'text' },
        placeholder: { control: 'text' }
    }
};

const Template = args => ({Component: Input, props: args});

export const Default = Template.bind({});
Default.args = {
    label: 'Some label',
    name: 'some-name',
    type: 'text',
    placeholder: 'some placeholder'
};