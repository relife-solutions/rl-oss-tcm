export default {
    save: (s) => browser.storage.local.set({state:s}),
    get: () => browser.storage.local.get('state').then(o => o.state)
};