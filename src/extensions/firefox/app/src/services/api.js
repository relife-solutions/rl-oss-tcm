const apiSettings = {
    url: '',
    key: ''
};

const up = browser.storage.local.get('apiUrl').then(u => apiSettings.url = u.apiUrl);
const kp = browser.storage.local.get('apiKey').then(u => apiSettings.key = u.apiKey);

const settings = Promise.all([up, kp]);

function url(...parts) {
    return parts.join('/');
}

/**
 * 
 * @param {string} method 
 * @param {string} url 
 * @param {any} body 
 * @returns {Promise<Response>}
 */
async function callApi(method, urlPart, body) {
    const _ = await settings;
    return await fetch(url(apiSettings.url, urlPart), {
        body: body,
        method: method
    });
}

const urls = {
    cases: {
        api: 'api/cases'
    },
    caseRuns: {
        api: 'api/runs'
    }
};

export default {
    get: {
        cases: () => callApi('GET', urls.cases.api),
        case: (id) => callApi('GET', url(urls.cases.api, id))
    },
    create: {
        case: (item) => callApi('POST', urls.cases.api, item),
        caseRun: (caseId, stepId, result) => callApi('POST', urls.caseRuns.api, {
            testCaseId: caseId,
            testStepId: stepId,
            status: result
        })
    },
    update: {
        case: (item) => callApi('PUT', urls.cases.api, item)
    },
    delete: {
        case: (id) => callApi('DELETE', url(urls.cases.api, id))
    }
};