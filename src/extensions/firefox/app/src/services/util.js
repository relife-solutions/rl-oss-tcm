export default {
    /**
     * 
     * @param {function():void} handler 
     * @returns {function(KeyboardEvent):void}
     */
    onkeyevent: handler => e => {
        (e.key === ' ' || e.key === 'Enter') && handler();
    }
};