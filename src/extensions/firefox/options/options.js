const apiUrl = 'apiUrl', apiKey = 'apiKey';

function getElement(id) {
  return document.querySelector(`#${id}`);
}

function saveOptions(e) {
  browser.storage.local.set({
    apiKey: getElement(apiKey).value,
    apiUrl: getElement(apiUrl).value
  });
  e.preventDefault();
}

function restoreOptions() {
  browser.storage.local.get(apiUrl).then(url => {
    getElement(apiUrl).value = url.apiUrl;
  });
  browser.storage.local.get(apiKey).then(key => {
    getElement(apiKey).value = key.apiKey;
  });
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
