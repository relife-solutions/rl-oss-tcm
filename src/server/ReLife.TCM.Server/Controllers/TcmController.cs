using Microsoft.AspNetCore.Mvc;

namespace ReLife.TCM.Server.Controllers;

public class TcmController : ControllerBase
{
    protected Func<Task<IActionResult>> Async<R>(Func<R> responder) where R : IActionResult =>
        () => Task.FromResult<IActionResult>(responder());
    
    protected Func<V, Task<IActionResult>> Async<V, R>(Func<object, R> responder) where R : IActionResult =>
        v => Task.FromResult<IActionResult>(responder(v));

    protected Func<IActionResult> Wipe<T>(Func<T> f) where T : IActionResult => () => f();  
    protected Func<V, IActionResult> Wipe<V, T>(Func<object, T> f) where T : IActionResult => v => f(v);  
    
    protected IActionResult UntypedOk<T>(T value) => Ok(value);
    protected IActionResult EmptyOk() => Ok();
    protected IActionResult EmptyOk<T>(T _) => Ok();
    protected Task<IActionResult> EmptyOkAsync() => Async(Ok)();
    protected Task<IActionResult> OkAsync<T>(T value) => Async<T, OkObjectResult>(Ok)(value);
}