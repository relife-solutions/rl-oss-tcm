using LanguageExt;
using Microsoft.AspNetCore.Mvc;
using ReLife.TCM.Extensions;
using ReLife.TCM.Models;
using ReLife.TCM.Server.Models.Request;
using ReLife.TCM.Services;

namespace ReLife.TCM.Server.Controllers;

[ApiController, Route("api/cases")]
public class Cases : TcmController
{
    private readonly TestCaseCrudService service;

    public Cases(TestCaseCrudService service)
    {
        this.service = service;
    }

    [HttpGet("{id}")]
    public Task<IActionResult> Get(string id) =>
        service.Read(id).MatchAsync(UntypedOk, Async(NotFound));

    [HttpGet]
    public Task<IActionResult> Get([FromQuery] CasesFilter filter)
    {
        // todo: filter usage
        return service.GetAll().Map(UntypedOk);
    }

    [HttpPost]
    public Task<IActionResult> Create([FromBody] TestCase item) => 
        service.Create(item).MatchAsync(UntypedOk, BadRequest);

    [HttpPut]
    public Task<IActionResult> Update([FromBody] TestCase item) => 
        service.Update(item).MatchAsync(EmptyOk, BadRequest);

    [HttpDelete]
    public Task<IActionResult> Delete(string id) =>
        service.MarkDeleted(id).MatchAsync(EmptyOk, BadRequest);
}