using Microsoft.AspNetCore.Mvc;
using ReLife.TCM.Extensions;
using ReLife.TCM.Models;
using ReLife.TCM.Server.Models.Request;
using ReLife.TCM.Services;

namespace ReLife.TCM.Server.Controllers;

[Route("api/runs"), ApiController]
public class CaseRuns : TcmController
{
    private readonly TestCaseRunCrudService service;

    public CaseRuns(TestCaseRunCrudService service)
    {
        this.service = service;
    }

    [HttpPost]
    public Task<IActionResult> Create(CreateCaseRun item) => service.Create(item.ToCaseRunResult()).MatchAsync(EmptyOk, BadRequest);
}