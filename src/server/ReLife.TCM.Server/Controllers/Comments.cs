using LanguageExt;
using Microsoft.AspNetCore.Mvc;
using ReLife.TCM.Models;
using ReLife.TCM.Services;
using ReLife.TCM.Extensions;

namespace ReLife.TCM.Server.Controllers;

[ApiController, Route("api/comment")]
public class Comments : TcmController
{
    private readonly TestCaseCrudService service;

    public Comments(TestCaseCrudService service)
    {
        this.service = service;
    }

    [HttpPost("case/{id}")]
    public Task<IActionResult> CommentCase(string id, [FromBody] Comment comment)
    {
        return service.AddComment(id, comment)
            .MatchAsync(UntypedOk, BadRequest);
    }
}