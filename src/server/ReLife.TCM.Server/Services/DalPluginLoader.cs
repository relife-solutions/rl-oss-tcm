using ReLife.TCM.DAL.Contracts;
using ReLife.TCM.DAL.Sqlite;
using Serilog;

namespace ReLife.TCM.Server.Services;

public static class DalPluginLoader
{
    public static IServiceCollection LoadDalProviderFrom(this IServiceCollection services, string assembly)
    {
        try
        {
            var entryPoints =
                assembly == "default"
                    ? Array.Empty<Type>()
                    : AppDomain.CurrentDomain.Load(assembly)
                        .ExportedTypes.Where(typeof(PluginEntryPoint).IsAssignableFrom).Take(2).ToArray();

            switch (entryPoints.Length)
            {
                case 1:
                    (Activator.CreateInstance(entryPoints[0]) as PluginEntryPoint)?.Load(services);
                    break;
                default:
                    if (assembly != "default")
                        Log.Warning("Assembly {assembly} exposes {count} plugin entry points, falling back to sqlite",
                            assembly, entryPoints.Length);
                    new SqliteEntryPoint().Load(services);
                    break;
            }
            return services;
        }
        catch (Exception e)
        {
            Log.Fatal(e, "Failed to load DAL plugin from {assembly}", assembly);
            throw;
        }
    }
}