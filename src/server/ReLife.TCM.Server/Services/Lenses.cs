using LanguageExt;
using ReLife.TCM.Extensions;
using ReLife.TCM.Models;
using ReLife.TCM.Server.Models.Request;

namespace ReLife.TCM.Server.Services;

public static class Lenses
{
    public static class CaseRunResult
    {
        public static readonly Lens<CreateCaseRun, string> TestCaseId =
            MutLenses<CreateCaseRun>.For(x => x.TestCaseId)!.Value;
        public static readonly Lens<CreateCaseRun, string> TestStepId =
            MutLenses<CreateCaseRun>.For(x => x.TestStepId)!.Value;
        public static readonly Lens<CreateCaseRun, CaseRunStatus> Status =
            MutLenses<CreateCaseRun>.For(x => x.Status)!.Value;
    }
}