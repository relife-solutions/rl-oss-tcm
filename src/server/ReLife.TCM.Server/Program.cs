using Microsoft.EntityFrameworkCore;
using ReLife.TCM.Server.Services;
using ReLife.TCM.Server.Validation.Rules;
using ReLife.TCM.Services;
using ReLife.TCM.Services.Validation;
using Serilog;

Log.Logger = new LoggerConfiguration()
    .WriteTo.File("rl.oss.tcm.log", fileSizeLimitBytes:32 * 1024, flushToDiskInterval: TimeSpan.FromSeconds(5), rollOnFileSizeLimit:true)
    .WriteTo.Console()
    .CreateLogger();

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.LoadDalProviderFrom(builder.Configuration["DAL"])
    .AddScoped<TestCaseCrudService>()
    .AddScoped<TestCaseRunCrudService>()
    .AddScoped<ReportService>()
    .AddSingleton<Validator>()
    .AddSingleton<RulesContainer, TestCaseRules>()
    .AddSingleton<RulesContainer, CaseRunRules>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

#if DEBUG
app.Services.GetService<DbContext>().Database.EnsureCreated();
#endif

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(builder =>
{
    builder.AllowAnyOrigin().AllowAnyMethod();
});

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
