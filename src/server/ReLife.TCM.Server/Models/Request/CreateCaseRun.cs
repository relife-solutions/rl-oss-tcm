using ReLife.TCM.Models;

namespace ReLife.TCM.Server.Models.Request;

public class CreateCaseRun
{
    public string TestCaseId { get; set; }
    public string TestStepId { get; set; }
    public CaseRunStatus Status { get; set; }

    public CaseRunResult ToCaseRunResult() => new()
    {
        Status = Status,
        TestCaseId = TestCaseId,
        TestStepId = TestStepId
    };
}