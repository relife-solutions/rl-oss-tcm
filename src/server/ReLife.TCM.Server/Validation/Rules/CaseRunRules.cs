using ReLife.TCM.Models;
using ReLife.TCM.Server.Models.Request;
using ReLife.TCM.Services.Validation;
using static LanguageExt.Prelude;
using static ReLife.TCM.Server.Services.Lenses.CaseRunResult;

namespace ReLife.TCM.Server.Validation.Rules;

public class CaseRunRules : RulesContainerBase<CreateCaseRun>
{
    public CaseRunRules()
    {
        Check(not(compose(TestCaseId.Get, string.IsNullOrWhiteSpace)), 
            "TestCaseId should not be empty");
        Check(not(compose(TestStepId.Get, string.IsNullOrWhiteSpace)), 
            "TestStepId should not be empty");
        Check(compose(Status.Get, Enum.IsDefined), "Status should be on of allowed");
        Check(compose(Status.Get, s => s != CaseRunStatus.None), "Status should be one of allowed");
    }
}