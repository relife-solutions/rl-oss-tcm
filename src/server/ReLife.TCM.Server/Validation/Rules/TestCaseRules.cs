using ReLife.TCM.Models;
using ReLife.TCM.Services.Validation;

namespace ReLife.TCM.Server.Validation.Rules;

public class TestCaseRules : RulesContainerBase<TestCase>
{
    public TestCaseRules()
    {
        Check(x => !x.IsDeleted,
            "Can't create case in deleted state");
        Check(x => !string.IsNullOrWhiteSpace(x.Title),
            "Can't create test with no title");
        Check(x => x.Steps.Any(),
            "Test require at least one step");
        Check(x => x.Steps.All(s =>
            !string.IsNullOrWhiteSpace(s.Precondition) && !string.IsNullOrWhiteSpace(s.Postcondition)),
            "All pre- and post-conditions in steps should be non-empty");
        Check(x => x.Id is null || !string.IsNullOrWhiteSpace(x.Id),
            "If Id is set, it should not be empty");
    }
}