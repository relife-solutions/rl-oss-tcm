﻿using LanguageExt;

namespace ReLife.TCM.Extensions;

public static class MonadicExtensions
{
    public static Task<Option<T>> DoAsync<T>(this Task<Option<T>> optionTask, Action<T> action) =>
        optionTask.Do(option => option.Do(action));
    
    public static Task<Option<T>> DoAsync<T, D>(this Task<Option<T>> optionTask, Func<T, D> action) =>
        optionTask.Do(option => option.Do(v => action(v)));

    public static Task<Either<L, T>> ToEither<L, T>(this Task<Option<T>> task, Func<L> left) =>
        task.Map(opt => opt.ToEither(left));

    public static Task<F> MatchAsync<L, R, F>(this Task<Either<L, R>> task, Func<R, F> onRight, Func<L, F> onLeft) =>
        task.Map(e => e.Match(onRight, onLeft));

    public static async Task<Either<L, B>> BindRightAsync<L, R, B>(this Either<L, R> either,
        Func<R, Task<Option<B>>> binder, Func<L> ifNone) =>
        !either.IsRight
            ? Either<L, B>.Left(either.LeftAsEnumerable().First())
            : (await binder(either.RightAsEnumerable().First())).ToEither(ifNone);
}

public static class FactoryMethodsExtensions
{
    public static T New<T>() where T : new() => new T();
}