using System.Linq.Expressions;
using System.Reflection;
using LanguageExt;

namespace ReLife.TCM.Extensions;

internal static class Lenses
{
    private static readonly Dictionary<string, object> lenses = new();
    
    private static string FullName<T>(string name) => $"{typeof(T).FullName}.{name}"; 

    public static Lens<T, P> GetOrAdd<T, P>(string name, Func<Lens<T, P>> factory)
    {
        var fullName = FullName<T>(name);
        if (lenses.TryGetValue(fullName, out var l) && l is Lens<T, P> lens)
            return lens;

        lens = factory();
        lenses[fullName] = lens;
        return lens;
    }

    public static Lens<T, P> ToLens<T, P>(this MemberExpression accessor)
    {
        Func<Expression, Expression> member =
            accessor.Member is PropertyInfo pi
                ? ex => Expression.Property(ex, pi)
                : ex => Expression.Field(ex, accessor.Member.Name);

        var sourceParameter = Expression.Parameter(typeof(T), "source");
        var readExpression = Expression.Lambda<Func<T, P>>(
            member(sourceParameter),
            sourceParameter);

        var valueParameter = Expression.Parameter(typeof(P), "value");
        var writeExpression = Expression.Lambda<Func<T, P, P>>(
            Expression.Assign(member(sourceParameter), valueParameter),
            sourceParameter, valueParameter);
        
        return Lens<T, P>.New(readExpression.Compile(), CreateSetter(writeExpression.Compile()));
    }

    private static Func<P, Func<T, T>> CreateSetter<T, P>(Func<T, P, P> setter)
    {
        return p => t =>
        {
            _ = setter(t, p);
            return t;
        };
    }
}

public static class MutLenses<T>
{
    public static Lens<T, P>? For<P>(Expression<Func<T, P>> accessor)
    {
        if (accessor.Body is not MemberExpression mx
            || mx.Member is not PropertyInfo or FieldInfo) return null;
        var name = mx.Member.Name;

        return Lenses.GetOrAdd(name, mx.ToLens<T, P>);
    }
}