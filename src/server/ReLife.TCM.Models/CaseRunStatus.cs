namespace ReLife.TCM.Models;

public enum CaseRunStatus
{
    None,
    Passed,
    Blocked,
    Failed
}