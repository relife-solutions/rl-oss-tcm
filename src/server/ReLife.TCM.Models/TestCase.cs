﻿namespace ReLife.TCM.Models;

public class TestCase
{
    public string? Id { get; set; }
    public string Title { get; set; }
    public string[] TestData { get; set; }
    public TestStep[] Steps { get; set; }
    public string Precondition { get; set; }
    
    public string[] Tags { get; set; }
    public string Url { get; set; }
    public List<Comment> Comments { get; set; }
    
    public bool IsDeleted { get; set; }
}