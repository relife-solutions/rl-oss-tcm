namespace ReLife.TCM.Models;

public class Comment
{
    public string? Id { get; set; }
    public DateTimeOffset CreatedAt { get; set; }
    public string Text { get; set; }
}