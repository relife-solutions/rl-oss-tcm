namespace ReLife.TCM.Models;

public class CaseRunResult
{
    public string? Id { get; set; }
    public string TestCaseId { get; set; }
    public string TestStepId { get; set; }
    public CaseRunStatus Status { get; set; }
    public string CommitSha { get; set; }
    public Comment[] Comments { get; set; }
}