namespace ReLife.TCM.Models;

public class TestStep
{
    public string? Id { get; set; }
    public string Precondition { get; set; }
    public string Postcondition { get;set; }
}