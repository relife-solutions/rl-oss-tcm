using LanguageExt;
using ReLife.TCM.Models;

namespace ReLife.TCM.DAL.Contracts;

public interface CaseRunRepository
{
    /// <summary>
    /// Read item by its id 
    /// </summary>
    /// <param name="id"></param>
    /// <returns>item or None</returns>
    Task<Option<CaseRunResult>> Get(string id);
    /// <summary>
    /// Read all items 
    /// </summary>
    /// <returns></returns>
    Task<CaseRunResult[]> GetAll();
    /// <summary>
    /// Get DB cursor allowing to query items on various criteria 
    /// </summary>
    /// <returns></returns>
    Task<IQueryable<CaseRunResult>> QueryAll();

    /// <summary>
    /// Create new case
    /// </summary>
    /// <param name="item">item to create. Id is ignored</param>
    /// <returns>id or exception observed while creating item</returns>
    Task<Either<Exception, string>> Create(CaseRunResult item);
    
    /// <summary>
    /// Add comment to case run
    /// </summary>
    /// <param name="item"></param>
    /// <returns>nothing (success) or exception</returns>
    Task<Either<Exception, Unit>> AddComment(string caseRunId, Comment comment);
}