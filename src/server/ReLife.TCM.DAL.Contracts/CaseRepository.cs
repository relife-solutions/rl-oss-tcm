﻿using LanguageExt;
using ReLife.TCM.Models;

namespace ReLife.TCM.DAL.Contracts;

public interface CaseRepository
{
    /// <summary>
    /// Read item by its id. Do not return item if it's marked as deleted 
    /// </summary>
    /// <param name="id"></param>
    /// <returns>item or None</returns>
    Task<Option<TestCase>> Get(string id);
    /// <summary>
    /// Read all non-deleted items 
    /// </summary>
    /// <returns></returns>
    Task<TestCase[]> GetAll();
    /// <summary>
    /// Get DB cursor or materialized enumerable cursor allowing to query items on various criteria. Allows to query deleted items 
    /// </summary>
    /// <returns></returns>
    IEnumerable<TestCase> QueryAll();

    /// <summary>
    /// Create new case
    /// </summary>
    /// <param name="item">item to create. Id is ignored</param>
    /// <returns>id or exception observed while creating item</returns>
    Task<Either<Exception, string>> Create(TestCase item);
    /// <summary>
    /// Update item given it's id
    /// </summary>
    /// <param name="item"></param>
    /// <returns>nothing (success) or exception</returns>
    Task<Either<Exception, Unit>> Update(TestCase item);
}