using Microsoft.Extensions.DependencyInjection;

namespace ReLife.TCM.DAL.Contracts;

public interface PluginEntryPoint
{
    void Load(IServiceCollection services);
}
