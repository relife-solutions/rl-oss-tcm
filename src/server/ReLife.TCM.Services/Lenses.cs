using LanguageExt;
using ReLife.TCM.Extensions;
using ReLife.TCM.Models;

namespace ReLife.TCM.Services;

public static class Lenses
{
    public static class CaseRunResult
    {
        public static readonly Lens<Models.CaseRunResult, string> TestCaseId =
            MutLenses<Models.CaseRunResult>.For(x => x.TestCaseId)!.Value;
        public static readonly Lens<Models.CaseRunResult, string> TestStepId =
            MutLenses<Models.CaseRunResult>.For(x => x.TestStepId)!.Value;
        public static readonly Lens<Models.CaseRunResult, string> CommitSha =
            MutLenses<Models.CaseRunResult>.For(x => x.CommitSha)!.Value;
        public static readonly Lens<Models.CaseRunResult, CaseRunStatus> Status =
            MutLenses<Models.CaseRunResult>.For(x => x.Status)!.Value;
        public static readonly Lens<Models.CaseRunResult, string> Id =
            MutLenses<Models.CaseRunResult>.For(x => x.Id)!.Value;
    }
}