﻿using LanguageExt;
using ReLife.TCM.DAL.Contracts;
using ReLife.TCM.Extensions;
using ReLife.TCM.Models;
using ReLife.TCM.Services.Validation;
using static ReLife.TCM.Extensions.FactoryMethodsExtensions;

namespace ReLife.TCM.Services;

public class ReportService
{
    
}

public class TestCaseCrudService
{
    private readonly CaseRepository cases;
    private readonly Validator validator;

    public TestCaseCrudService(CaseRepository cases, Validator validator)
    {
        this.cases = cases;
        this.validator = validator;
    }

    public Task<Option<TestCase>> Read(string id) => cases.Get(id);
    public Task<TestCase[]> GetAll() => cases.GetAll();


    public Task<Either<Exception, string>> Create(TestCase item) =>
        validator.Validate(item)
            .BindAsync(cases.Create);

    public Task<Either<Exception,Unit>> AddComment(string id, Comment comment)
    {
        return Read(id)
            .DoAsync(cs => cs.Comments.Add(comment))
            .Map(o => o.ToEither<Exception>(new KeyNotFoundException()))
            .BindAsync(cases.Update);
    }

    public Task<Either<Exception,Unit>> Update(TestCase item)
    {
        return validator.Validate(item)
            .Bind(i => i.Id is null ? (Either<Exception, TestCase>)new InvalidOperationException() : i)
            .BindRightAsync(i => Read(i.Id!), New<KeyNotFoundException>)
            .MapAsync(Prelude.constant<TestCase, TestCase>(item))
            .MapAsync(MutLenses<TestCase>.For(x => x.Comments).Value.SetF(null))
            .BindAsync(cases.Update);
    }

    public Task<Either<Exception, Unit>> MarkDeleted(string id)
    {
        var isDeleted = MutLenses<TestCase>.For(x => x.IsDeleted)!.Value;

        return Read(id)
            .DoAsync(isDeleted.SetF(true))
            .ToEither<Exception, TestCase>(New<KeyNotFoundException>)
            .BindAsync(cases.Update);
    }
}
