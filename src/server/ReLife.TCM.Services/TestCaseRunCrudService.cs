using LanguageExt;
using ReLife.TCM.DAL.Contracts;
using ReLife.TCM.Models;
using ReLife.TCM.Services.Validation;

namespace ReLife.TCM.Services;

public class TestCaseRunCrudService
{
    private readonly CaseRunRepository repository;
    private readonly Validator validator;

    public TestCaseRunCrudService(CaseRunRepository repository, Validator validator)
    {
        this.repository = repository;
        this.validator = validator;
    }

    public Task<Either<Exception,Unit>> Create(CaseRunResult item)
    {
        return validator.Validate(item)
            .BindAsync(repository.Create)
            .MapAsync(Prelude.ignore);
    }
}