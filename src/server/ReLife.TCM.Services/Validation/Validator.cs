using LanguageExt;

namespace ReLife.TCM.Services.Validation;

public class Validator
{
    private readonly Dictionary<Type, RulesContainer[]> rules;

    public Validator(IEnumerable<RulesContainer> containers)
    {
        rules = containers.GroupBy(x => x.TargetType).ToDictionary(x => x.Key, x => x.ToArray());
    }

    public Either<Exception, T> Validate<T>(T item, bool permissive = false)
    {
        if (rules.TryGetValue(typeof(T), out var tRules)) {
            var typedRules = tRules.OfType<RulesContainer<T>>().ToArray();
            if (typedRules.Length != 0) {
                var ex = typedRules.Select(x => x.Validate(item)).FirstOrDefault(x => x.IsSome);
                return ex.Match<Either<Exception, T>>(e => e, () => item);
            }
        }

        if (permissive)
            return item;
        return new KeyNotFoundException($"Type {typeof(T)} was not configured for validation");
    }
}