using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using LanguageExt;

namespace ReLife.TCM.Services.Validation;

public abstract class RulesContainerBase<T> : RulesContainer<T>
{
    private readonly List<(Func<T, bool>, string)> rules = new();

    public Type TargetType { get; } = typeof(T);

    protected void Check(Func<T, bool> predicate, string error)
    {
        if (predicate is not null && !string.IsNullOrWhiteSpace(error))
            rules.Add((predicate, error));
    }

    protected void Check(Expression<Func<T, bool>> predicate)
    {
        if (predicate is not null)
            rules.Add((predicate.Compile(), predicate.ToString()));
    }

    public Option<Exception> Validate(T value)
    {
        var error = rules.FirstOrDefault(x => !x.Item1(value)).Item2;
        return string.IsNullOrWhiteSpace(error) 
            ? Option<Exception>.None 
            : (Option<Exception>)new ValidationException(error);
    }
}