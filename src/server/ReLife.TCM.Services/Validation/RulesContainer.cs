using LanguageExt;

namespace ReLife.TCM.Services.Validation;

public interface RulesContainer { 
    Type TargetType { get; }
}

public interface RulesContainer<T> : RulesContainer
{
    Option<Exception> Validate(T value);
}