namespace ReLife.TCM.DAL.Sqlite.Models;

internal class SqliteStep
{
    public long Id { get; set; }
    public string Precondition { get; set; }
    public string Postcondition { get;set; }   
}