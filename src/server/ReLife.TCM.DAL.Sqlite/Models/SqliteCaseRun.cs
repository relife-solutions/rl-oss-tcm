using ReLife.TCM.Models;

namespace ReLife.TCM.DAL.Sqlite.Models;

internal class SqliteCaseRun
{
    public long Id { get; set; }
    public long TestCaseId { get; set; }
    public CaseRunStatus Status { get; set; }
    public string CommitSha { get; set; }
    
    public virtual ICollection<SqliteComment> Comments { get; set; }
    public virtual SqliteTestCase TestCase { get; set; }
    public virtual SqliteStep TestStep { get; set; }
}