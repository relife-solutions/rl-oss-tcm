namespace ReLife.TCM.DAL.Sqlite.Models;

internal class SqliteComment
{
    public long Id { get; set; }
    public DateTimeOffset CreatedAt { get; set; }
    public string Text { get; set; }
}