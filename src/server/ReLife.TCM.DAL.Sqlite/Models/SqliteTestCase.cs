namespace ReLife.TCM.DAL.Sqlite.Models;

internal class SqliteTestCase
{
    public long Id { get; set; }
    public string Title { get; set; }
    public string JsonTestData { get; set; }
    public string Precondition { get; set; }
    public bool IsDeleted { get; set; }
    
    public string JsonTags { get; set; }
    public string Url { get; set; }
    
    public virtual ICollection<SqliteComment> Comments { get; set; }
    public virtual ICollection<SqliteStep> Steps { get; set; }
}