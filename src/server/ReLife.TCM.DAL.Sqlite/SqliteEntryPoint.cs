﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ReLife.TCM.DAL.Contracts;

namespace ReLife.TCM.DAL.Sqlite;
public class SqliteEntryPoint : PluginEntryPoint
{
    public void Load(IServiceCollection services)
    {
        services.AddScoped<TcmContext>()
#if DEBUG
            .AddTransient<DbContext, TcmContext>()
#endif
            .AddScoped<CaseRunRepository, EfCaseRunRepository>()
            .AddScoped<CaseRepository, EfCaseRepository>();
    }
}