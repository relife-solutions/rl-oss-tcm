using LanguageExt;
using Microsoft.EntityFrameworkCore;
using ReLife.TCM.DAL.Sqlite.Models;

namespace ReLife.TCM.DAL.Sqlite;

internal class TcmContext : DbContext
{
    public TcmContext() : base(new DbContextOptionsBuilder<TcmContext>().UseSqlite("Data Source=tcm.default.db3").Options)
    {
        ChangeTracker.AutoDetectChangesEnabled = false;
        ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        ChangeTracker.LazyLoadingEnabled = true;
    }
    
    public DbSet<SqliteTestCase> Cases { get; set; }
    public DbSet<SqliteComment> Comments { get; set; }
    public DbSet<SqliteCaseRun> Runs { get; set; }
    public DbSet<SqliteStep> Steps { get; set; }

    protected override void OnModelCreating(ModelBuilder m)
    {
        m.Entity<SqliteTestCase>().Navigation(x => x.Comments).AutoInclude();
        m.Entity<SqliteTestCase>().Navigation(x => x.Steps).AutoInclude();
        m.Entity<SqliteCaseRun>().Navigation(x => x.Comments).AutoInclude();
        m.Entity<SqliteCaseRun>().Navigation(x => x.TestCase).AutoInclude();
        
        base.OnModelCreating(m);
    }
}