using LanguageExt;
using ReLife.TCM.DAL.Contracts;
using ReLife.TCM.Models;

namespace ReLife.TCM.DAL.Sqlite;

internal class EfCaseRunRepository : CaseRunRepository
{
    private readonly TcmContext context;

    public EfCaseRunRepository(TcmContext context)
    {
        this.context = context;
    }

    public Task<Option<CaseRunResult>> Get(string id)
    {
        throw new NotImplementedException();
    }

    public Task<CaseRunResult[]> GetAll()
    {
        throw new NotImplementedException();
    }

    public Task<IQueryable<CaseRunResult>> QueryAll()
    {
        throw new NotImplementedException();
    }

    public Task<Either<Exception, string>> Create(CaseRunResult item)
    {
        throw new NotImplementedException();
    }

    public Task<Either<Exception, Unit>> AddComment(string caseRunId, Comment comment)
    {
        throw new NotImplementedException();
    }
}