using LanguageExt;
using ReLife.TCM.DAL.Contracts;
using ReLife.TCM.Models;
using Serilog;
// ReSharper disable InconsistentLogPropertyNaming

namespace ReLife.TCM.DAL.Sqlite;

internal class EfCaseRepository : CaseRepository
{
    private readonly TcmContext context;

    public EfCaseRepository(TcmContext context)
    {
        this.context = context;
    }

    public Task<Option<TestCase>> Get(string id)
    {
        if (!long.TryParse(id, out var cid))
        {
            Log.Warning("Attempt to find test case with non-numeric id {id}", id);
            return Task.FromResult(Option<TestCase>.None);
        }

        try
        {
            if (context.Cases.SingleOrDefault(x => x.Id == cid) is { } t)
                return Task.FromResult<Option<TestCase>>(Mapper.ToTestCase(t));
            return Task.FromResult(Option<TestCase>.None);
        }
        catch (Exception e)
        {
            Log.Error(e, "Failed to read test case {id}", cid);
            return Task.FromResult(Option<TestCase>.None);
        }
    }

    public Task<TestCase[]> GetAll()
    {
        try
        {
            return Task.FromResult((context.Cases.Where(x => !x.IsDeleted).ToList()).Select(Mapper.ToTestCase).ToArray());
        }
        catch (Exception e)
        {
            Log.Error(e, "Failed to read test cases");
            return Task.FromResult(Array.Empty<TestCase>());
        }
    }

    public IEnumerable<TestCase> QueryAll() => context.Cases.Select(Mapper.ToTestCase);

    public async Task<Either<Exception, string>> Create(TestCase item)
    {
        try
        {
            var test = Mapper.FromTestCase(item);
            await context.Cases.AddAsync(test);
            await context.SaveChangesAsync();
            return test.Id.ToString();
        }
        catch (Exception e)
        {
            Log.Error(e, "Failed to create test case {title}", item.Title);
            return e;
        }
    }

    public async Task<Either<Exception, Unit>> Update(TestCase item)
    {
        try
        {
            context.Cases.Update(Mapper.FromTestCase(item));
            await context.SaveChangesAsync();
            return Unit.Default;
        }
        catch (Exception e)
        {
            Log.Error(e, "Failed to update test case {id}", item.Id);
            return e;
        }
    }
}