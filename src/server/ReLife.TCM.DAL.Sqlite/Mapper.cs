using Newtonsoft.Json;
using ReLife.TCM.DAL.Sqlite.Models;
using ReLife.TCM.Models;

namespace ReLife.TCM.DAL.Sqlite;

internal static class Mapper
{
    public static Comment ToComment(SqliteComment source) => new()
    {
        Id = source.Id.ToString(),
        CreatedAt = source.CreatedAt,
        Text = source.Text
    };
    
    public static SqliteComment FromComment(Comment source) => new()
    {
        Id = long.Parse(source.Id ?? "0"),
        CreatedAt = source.CreatedAt,
        Text = source.Text
    };

    public static TestStep ToStep(SqliteStep source) => new()
    {
        Id = source.Id.ToString(),
        Postcondition = source.Postcondition,
        Precondition = source.Precondition
    };

    public static CaseRunResult ToCaseRun(SqliteCaseRun run) => new();

    public static SqliteStep FromStep(TestStep source) => new()
    {
        Id = long.Parse(source.Id ?? "0"),
        Postcondition = source.Postcondition,
        Precondition = source.Precondition
    };
    
    public static TestCase ToTestCase(SqliteTestCase source) => new()
    {
        Comments = source.Comments?.Select(ToComment).ToList() ?? new(),
        Id = source.Id.ToString(),
        Precondition = source.Precondition,
        Steps = source.Steps?.Select(ToStep).ToArray() ?? Array.Empty<TestStep>(),
        Tags = JsonConvert.DeserializeObject<string[]>(source.JsonTags)!,
        Title = source.Title,
        Url = source.Url,
        IsDeleted = source.IsDeleted,
        TestData = JsonConvert.DeserializeObject<string[]>(source.JsonTestData)!
    };


    public static SqliteTestCase FromTestCase(TestCase source) =>
        new()
        {
            Comments = source.Comments?.Select(FromComment).ToList() ?? new (),
            Id = long.Parse(source.Id ?? "0"),
            Precondition = source.Precondition,
            Steps = source.Steps?.Select(FromStep).ToArray() ?? Array.Empty<SqliteStep>(),
            JsonTags = JsonConvert.SerializeObject(source.Tags),
            Title = source.Title,
            Url = source.Url,
            IsDeleted = source.IsDeleted,
            JsonTestData = JsonConvert.SerializeObject(source.TestData)
        };

    public static SqliteCaseRun FromCaseRun(CaseRunResult run) => new();
}